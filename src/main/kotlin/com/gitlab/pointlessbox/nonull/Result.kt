package com.gitlab.pointlessbox.nonull

import java.util.concurrent.CancellationException
import kotlin.reflect.KClass

/**
 * Represents an action that can succeed or fail.
 *
 * ## Example
 *
 * ```kotlin
 * // Declare typealias to be less verbose
 * typealias ExResult<T> = Result<T, Throwable>
 *
 * fun mayFail(): ExResult<Int> {
 *     return try {
 *        // Some action that might fail
 *        Result.Ok(/* some value */)
 *     } catch (ex: Throwable) {
 *       Result.Err(ex)
 *     }
 * }
 *
 * when (val maybeInt = mayFail()) {
 *   is Result.Ok -> {
 *     val okInt = maybeInt.value
 *     // do something with [okInt]
 *   }
 *   is Result.Err -> {
 *     val err = maybeInt.err
 *     // do something with [err]
 *   }
 * }
 * ```
 */
sealed class Result<out T, out E> {
    /**
     * Represents a succeeded action.
     * For more details see [Result].
     */
    data class Ok<T>(val value: T) : Result<T, Nothing>()

    /**
     * Represents a failed action.
     * For more details see [Result].
     */
    data class Err<E>(val err: E) : Result<Nothing, E>()

    /** Tells whether this is a [Result.Ok] or not. */
    fun isOk(): Boolean = this is Ok

    /** Tells whether this is an [Result.Err] or not. */
    fun isErr(): Boolean = !this.isOk()

    /**
     * Executes the given [onOk] closure when this is [Result.Ok].
     * [onOk] can return a value which is wrapped in an [Option.Some] when executed.
     * Returns [Option.None] otherwise.
     */
    inline fun <O> ifOk(onOk: (T) -> O): Option<O> = when (this) {
        is Ok<T> -> Option.Some(onOk(this.value))
        else -> Option.None
    }

    /**
     * Executes the given [onErr] closure when this is [Result.Err].
     * [onErr] can return a value which is wrapped in a new [Option.Some] when executed.
     * Returns [Option.None] otherwise.
     */
    inline fun <O> ifErr(onErr: (E) -> O): Option<O> = when (this) {
        is Err -> Option.Some(onErr(this.err))
        else -> Option.None
    }

    /**
     * Creates a [Option] from this [Result].
     * Shorthand for: `this.ifOk { it }`.
     */
    fun toOption(): Option<T> = this.ifOk { it }
}

/** Shorthand for [Result.Ok]. */
typealias Ok<T> = Result.Ok<T>

/** Shorthand for [Result.Err]. */
typealias Err<E> = Result.Err<E>

/** Wraps [this] in [Ok]. */
fun <T> T.ok(): Ok<T> = Ok(this)

/** Wraps [this] in [Err]. */
fun <E> E.err(): Err<E> = Err(this)