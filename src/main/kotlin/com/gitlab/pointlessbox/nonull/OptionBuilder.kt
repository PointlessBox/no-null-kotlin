@file:JvmName("OptionBuilder")

package com.gitlab.pointlessbox.nonull

import java.lang.NullPointerException

/**
 * Allows to safely use non-null-assertion in [builder].
 *
 * ### Example
 *
 * ```kotlin
 * val addition = option {
 *     1 + someNullableNumber!!
 * }
 * ```
 */
inline fun <T> option(builder: () -> T): Option<T> = try {
    Some(builder())
} catch (ex: NullPointerException) {
    None
}