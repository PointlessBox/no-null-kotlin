package com.gitlab.pointlessbox.nonull

/**
 * Represents a value that can be either something or nothing.
 *
 * ## Example
 *
 * ```kotlin
 * fun makeSafe(nullableInt: Int?): Option<Int> {
 *   return if (nullableInt != null) Option.Some(nullableInt)
 *   else Option.None
 * }
 *
 * when (val maybeInt = makeSafe(/* some nullable int */)) {
 *   is Option.Some -> {
 *     val someInt = maybeInt.value
 *     // do something with [someInt]
 *   }
 *   is Option.None -> {
 *     // do something else
 *   }
 * }
 * ```
 */
sealed class Option<out T> : Iterable<T> {

    /**
     * Represents a non-null value.
     * For more details see [Option].
     */
    data class Some<T>(val value: T) : Option<T>() {
        override fun iterator(): Iterator<T> = object : Iterator<T> {
            private var valueWasRead = false

            override fun hasNext(): Boolean = !valueWasRead

            override fun next(): T {
                valueWasRead = true
                return value
            }
        }
    }

    /**
     * Represents a null value.
     * For more details see [Option].
     */
    object None : Option<Nothing>() {
        override fun iterator(): Iterator<Nothing> = object : Iterator<Nothing> {
            override fun hasNext(): Boolean = false

            override fun next(): Nothing = throw NoSuchElementException(
                "${None::class.qualifiedName} can not hold any elements. So calling next() on its iterator will always throw."
            )
        }
    }

    companion object {
        /**
         * Creates an [Option] from the given nullable [maybeValue].
         */
        @JvmStatic
        fun <T> from(maybeValue: T?): Option<T> = when (maybeValue) {
            null -> None
            else -> Some(maybeValue)
        }
    }

    /**
     * Tells whether this is an [Option.Some] or not
     */
    fun isSome(): Boolean = this is Some

    /**
     * Tells whether this is an [Option.None] or not
     */
    fun isNone(): Boolean = !this.isSome()

    /**
     * Executes the given [onSome] closure when this is [Option.Some].
     * [onSome] can return a value which is wrapped in a new [Option] when executed.
     * Returns [Option.Some] otherwise
     */
    inline fun <O> ifSome(onSome: (T) -> O): Option<O> = when (this) {
        is Some<T> -> Some(onSome(this.value))
        else -> None
    }

    /**
     * Executes the given [onNone] closure when this is [Option.None].
     * [onNone] can return a value which is wrapped in a new [Option] when executed.
     * Returns [Option.None] otherwise
     */
    inline fun <O> ifNone(onNone: () -> O): Option<O> = when (this) {
        is None -> Some(onNone())
        else -> None
    }

    /**
     * Creates a [Result] from this [Option]. If this is [None], then
     * [Err] is called with [None] as parameter.
     */
    fun toResult(): Result<T, None> = when (this) {
        is Some -> Ok(this.value)
        is None -> Err(None)
    }

    /**
     * Creates a [Result] from this [Option].
     */
    fun <E> toResult(err: E) = when (this) {
        is Some -> Ok(this.value)
        is None -> Err(err)
    }
}

/**
 * Shorthand for [Option.Some].
 */
typealias Some<T> = Option.Some<T>

/**
 * Shorthand for [Option.None]
 */
typealias None = Option.None

/**
 * Factory for [Option] which returns [Some] if [value] != null,
 * and [None] otherwise.
 */
fun <T> maybe(value: T?): Option<T> = when {
    value != null -> Some(value)
    else -> None
}

/** Wraps [this] in [Some] */
fun <T> T.some(): Some<T> = Some(this)

/** Wraps this in [Option] */
fun <T> T?.option(): Option<T> = maybe(this)