@file:JvmName("ResultBuilder")

package com.gitlab.pointlessbox.nonull

import java.util.concurrent.CancellationException
import kotlin.reflect.KClass

/**
 * Wraps the result of [builder] as [Ok]. Catches all [Exception]s except
 * for [CancellationException].
 */
inline fun <T> result(
    builder: () -> T,
): Result<T, Exception> = try {
    Ok(builder())
} catch (ex: CancellationException) {
    throw ex
} catch (ex: Exception) {
    Err(ex)
}

/**
 * Wraps the result of [builder] as [Ok].
 * Catches all [Exception]s except [CancellationException] defined in [map] and maps them
 * to the specified value.
 */
inline fun <T, E> mapErr(
    vararg map: Pair<KClass<out Exception>, E>,
    builder: () -> T,
): Result<T, E> = result(builder).mapErr { ex ->
    map.toMap()[ex::class] ?: throw ex
}

/**
 * Wraps the result of [builder] as [Ok]. Catches all [Exception]s except
 * for [CancellationException] and the ones passed with [err].
 * Using [raise] without passing [err] is equal to using [result].
 */
inline fun <T> raise(
    vararg err: KClass<out Exception>,
    builder: () -> T,
): Result<T, Exception> = result(builder).mapErr { ex ->
    if (err.any { it.isInstance(ex) }) throw ex
    else ex
}

/**
 * Wraps the result of [builder] as [Ok].
 * Catches all [Exception]s of [err] except [CancellationException].
 */
inline fun <T> catch(
    vararg err: KClass<out Exception>,
    builder: () -> T,
): Result<T, Exception> = result(builder).mapErr { ex ->
    if (err.any { it.isInstance(ex) }) ex
    else throw ex
}

