@file:JvmName("OptionExt")
package com.gitlab.pointlessbox.nonull


/**
 * When this is [Option.Some] returns the inner value.
 * Otherwise, calls [default] and returns its returned value.
 */
fun <T> Option<T>.getOr(default: () -> T): T = when (this) {
    is Option.Some -> this.value
    else -> default()
}

/**
 * When this is [Option.Some] returns the inner value.
 * Otherwise, returns [default].
 */
fun <T> Option<T>.getOr(default: T): T = when (this) {
    is Option.Some -> this.value
    else -> default
}

/**
 * Filters all [Option.Some] returning a new [List] with only [Option.Some].
 */
fun <T> Iterable<Option<T>>.filterSome(): List<Option.Some<T>> =
    this.filterIsInstance<Option.Some<T>>()

/**
 * Filters all [Option.None] returning a new [List] with only [Option.None].
 */
fun <T> Iterable<Option<T>>.filterNone(): List<Option.None> =
    this.filterIsInstance<Option.None>()

/**
 * Filters all [Option.Some] instances and maps the inner values.
 */
fun <T> Iterable<Option<T>>.values(): List<T> =
    this.filterSome().map { it.value }

/**
 * Maps the [Some]-value if presents and returns it as a new [Option]
 */
inline fun <T, R> Option<T>.map(transform: (T) -> R): Option<R> =
    this.ifSome(transform)

/** Gets the inner [Option] if possible. */
fun <T> Option<Option<T>>.flatten(): Option<T> =
    if (this is Some) this.value
    else None

/** Applies [transform] if possible and [flatten]s the result. */
inline fun <T, R> Option<T>.flatMap(transform: (T) -> Option<R>): Option<R> =
    this.map(transform).flatten()