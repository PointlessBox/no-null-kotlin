@file:JvmName("ResultExt")

package com.gitlab.pointlessbox.nonull


/**
 * When this is [Result.Ok] returns the inner value.
 * Otherwise, calls [default] and returns its returned value.
 */
inline fun <T> Result<T, *>.getOr(default: () -> T): T = when (this) {
    is Result.Ok -> this.value
    else -> default()
}

/**
 * When this is [Result.Ok] returns the inner value.
 * Otherwise, returns [default].
 */
fun <T> Result<T, *>.getOr(default: T): T = when (this) {
    is Result.Ok -> this.value
    else -> default
}

/**
 * Filters all [Result.Ok] returning a new [List] with only [Result.Ok].
 */
fun <T, E> Iterable<Result<T, E>>.filterOk(): List<Result.Ok<T>> =
    this.filterIsInstance<Result.Ok<T>>()

/**
 * Filters all [Result.Err] returning a new [List] with only [Result.Err].
 */
fun <T, E> Iterable<Result<T, E>>.filterErr(): List<Result.Err<E>> =
    this.filterIsInstance<Result.Err<E>>()

/**
 * Filters all [Result.Ok] instances and maps the inner values.
 */
fun <T, E> Iterable<Result<T, E>>.values(): List<T> =
    this.filterOk().map { it.value }

/**
 * Filters all [Result.Err] instances and maps the inner errors.
 */
fun <T, E> Iterable<Result<T, E>>.errors(): List<E> =
    this.filterErr().map { it.err }

/**
 * Maps only the [Ok] value if present and returns a new [Result] with the
 * type of [Ok] being [R].
 */
inline fun <T, E, R> Result<T, E>.mapOk(transform: (T) -> R): Result<R, E> =
    if (this is Ok) Ok(transform(this.value))
    else Err((this as Err).err)

/**
 * Maps only the [Err] if present and returns a new [Result] with the
 * type of [Err] being [R].
 */
inline fun <T, E, R> Result<T, E>.mapErr(transform: (E) -> R): Result<T, R> =
    if (this is Ok) Ok(this.value)
    else Err(transform((this as Err).err))

/** Returns the Result on the [Ok] side if present, [Err] otherwise. */
fun <O, E> Result<Result<O, E>, E>.flattenOk(): Result<O, E> =
    if (this is Ok) this.value
    else this as Err

/** Returns the Result on the [Err] side if present, [Ok] otherwise. */
fun <O, E> Result<O, Result<O, E>>.flattenErr(): Result<O, E> =
    if (this is Err) this.err
    else this as Ok

/** Returns the Result on the [Err] side if present, [Ok] otherwise. */
fun <O, E> Result<Result<O, E>, Result<O, E>>.flatten(): Result<O, E> =
    if (this is Err) this.flattenErr()
    else (this as Ok).flattenOk()

/** Maps and flattens the [Ok] side. */
inline fun <O, E, ReturnO> Result<O, E>.flatMapOk(
    transform: (O) -> Result<ReturnO, E>,
): Result<ReturnO, E> = this.mapOk(transform).flattenOk()

/** Maps and flattens the [Err] side. */
inline fun <O, E, ReturnE> Result<O, E>.flatMapErr(
    transform: (E) -> Result<O, ReturnE>,
): Result<O, ReturnE> = this.mapErr(transform).flattenErr()