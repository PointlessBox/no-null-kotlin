package com.gitlab.pointlessbox.nonull.option_test

import com.gitlab.pointlessbox.nonull.*
import org.junit.Test
import kotlin.test.assertEquals

class OptionTest {
    @Test
    fun typeAliases_shouldProduceTheCorrectInstances() {
        assert(Some(1).isSome())
        assert(None.isNone())
    }

    @Test
    fun maybe_shouldProduceTheCorrectOptionVariants() {
        assert(maybe(1).isSome())
        assert(maybe(null).isNone())
    }

    @Test
    fun isSome_shouldReturnTrueWhenSomeOptionAndFalseIfNoneOption() {
        assert(someInt.isSome())
        assert(!noInt.isSome())
    }

    @Test
    fun isNone_shouldReturnFalseWhenSomeOptionAndFalseIfNoneOption() {
        assert(noInt.isNone())
        assert(!someInt.isNone())
    }

    @Test
    fun from_shouldProduceASome_whenGivenANonNullValue() {
        assert(someInt is Option.Some)
    }

    @Test
    fun from_shouldProduceANone_whenGivenANullValue() {
        assert(noInt is Option.None)
    }

    @Test
    fun ifSome_shouldAlwaysReturnAnOption() {
        val someFromNone = noInt.ifSome {
            1
        }
        val someFromSome = someInt.ifSome {
            2
        }
        assert(someFromNone is Option.None)
        assert(someFromSome is Option.Some<Int>)
    }

    @Test
    fun ifSome_shouldRun_whenCalledOnASomeOption() {
        var wasCalled = false
        someInt.ifSome {
            wasCalled = true
        }
        assert(wasCalled)
    }

    @Test
    fun ifSome_shouldNotRun_whenCalledOnANoneOption() {
        var wasCalled = false
        noInt.ifSome {
            // This should not run
            wasCalled = true
        }
        assert(!wasCalled)
    }

    @Test
    fun ifNone_shouldAlwaysReturnAnOption() {
        val someFromNone = noInt.ifNone {
            1
        }
        val someFromSome = someInt.ifNone {
            2
        }
        assert(someFromNone is Option.Some<Int>)
        assert(someFromSome is Option.None)
    }

    @Test
    fun ifNone_shouldNotRun_whenCalledOnASomeOption() {
        var wasCalled = false
        someInt.ifNone {
            // This should not run
            wasCalled = true
        }
        assert(!wasCalled)
    }

    @Test
    fun ifNone_shouldRun_whenCalledOnANoneOption() {
        var wasCalled = false
        noInt.ifNone {
            wasCalled = true
        }
        assert(wasCalled)
    }


    @Test
    fun toResult_shouldReturn_aOkResultWhenCalledOnSomeOption() {
        assert(someInt.toResult().isOk())
        assert(someInt.toResult(1).isOk())
    }

    @Test
    fun toResult_shouldReturn_aErrResultWhenCalledOnNoneOption() {
        assert(noInt.toResult().isErr())
        assert(noInt.toResult(1).ifErr { true }.getOr(false))
    }

    @Test
    fun getOr_shouldReturnTheInnerSomeValue_whenItIsASomeOption() {
        assertEquals(one, someInt.getOr { 2 })
        assertEquals(one, someInt.getOr(2))
    }

    @Test
    fun getOr_shouldReturnTheDefaultValue_whenItIsANoneOption() {
        val default = 2
        assertEquals(one, someInt.getOr { default })
        assertEquals(one, someInt.getOr(default))
    }

    @Test
    fun filterSome_shouldRemoveAllNoneOptions() {
        assert(optionList.filterSome().none { it.isNone() })
    }

    @Test
    fun filterSome_shouldNotRemoveAnySomeOptions() {
        assertEquals(someList.size, optionList.filterSome().size)
    }

    @Test
    fun filterNone_shouldRemoveAllSomeOptions() {
        assert(optionList.filterNone().none { it.isSome() })
    }

    @Test
    fun filterNone_shouldNotRemoveAnyNoneOptions() {
        assertEquals(noneList.size, optionList.filterNone().size)
    }

    @Test
    fun values_shouldReturnAllInnerValuesOfSomeInstances() {
        assertEquals(listOf(0, 1, 2), optionList.values())
    }
}
