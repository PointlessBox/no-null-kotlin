package com.gitlab.pointlessbox.nonull.option_test

import com.gitlab.pointlessbox.nonull.None
import com.gitlab.pointlessbox.nonull.Option
import com.gitlab.pointlessbox.nonull.flatMap
import com.gitlab.pointlessbox.nonull.maybe
import org.junit.Test
import kotlin.test.assertEquals

class OptionFlatMapTest {

    @Test
    fun given_aSomeOption_when_mappedToAnotherOption_then_theInnerOptionShouldBeReturned() {
        assertEquals(maybe(2), maybe(1).flatMap { maybe(it + 1) })
        assertEquals(None, maybe(1).flatMap { None })
    }

    @Test
    fun given_none_when_mappedToAnotherOption_then_itShouldStillBeNone() {
        assertEquals(None, (None as Option<Int>).flatMap { maybe(it + 1) })
    }
}