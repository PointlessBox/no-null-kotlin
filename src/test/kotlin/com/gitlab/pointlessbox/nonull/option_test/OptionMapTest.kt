package com.gitlab.pointlessbox.nonull.option_test

import com.gitlab.pointlessbox.nonull.Some
import com.gitlab.pointlessbox.nonull.map
import org.junit.Test
import kotlin.test.assertEquals

class OptionMapTest {
    @Test
    fun given_someValue_when_mapped_then_aNewOptionWithTheMappedValueShouldBeReturned() {
        val mapped = someInt.map { it.toString() }
        assertEquals(one.toString(), (mapped as Some).value)
    }
}