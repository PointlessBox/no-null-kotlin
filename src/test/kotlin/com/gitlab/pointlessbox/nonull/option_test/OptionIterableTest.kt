package com.gitlab.pointlessbox.nonull.option_test

import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails

class OptionIterableTest {
    @Test
    fun given_someValueAsOption_when_iteratingOverIt_then_itShouldReturnAValueOnce() {
        val iterator = someInt.iterator()
        assert(iterator.hasNext())
        assertEquals(iterator.next(), one)
        assert(!iterator.hasNext())
    }
    @Test
    fun given_aNoneOption_when_iteratingOverIt_then_returnNoValue() {
        val iterator = noInt.iterator()
        assert(!iterator.hasNext())
        assertFails { iterator.next() }
    }
}