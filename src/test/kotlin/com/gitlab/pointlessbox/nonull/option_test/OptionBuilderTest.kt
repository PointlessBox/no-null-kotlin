package com.gitlab.pointlessbox.nonull.option_test

import com.gitlab.pointlessbox.nonull.getOr
import com.gitlab.pointlessbox.nonull.option
import org.junit.Test
import kotlin.test.assertEquals

class OptionBuilderTest {

    @Test
    fun optionBuilder_shouldCatchNullPointerExceptions() {
        assert(option { null!! }.isNone())
    }

    @Test
    fun optionBuilder_shouldReturnSomeIfNoNullPointerExceptionOccured() {
        assertEquals(2, option { 1 + 1 }.getOr(0))
    }
}