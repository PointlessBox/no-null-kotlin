package com.gitlab.pointlessbox.nonull.option_test

import com.gitlab.pointlessbox.nonull.None
import com.gitlab.pointlessbox.nonull.Option

internal const val one = 1
internal var nullableInt: Int? = one
internal var nullInt: Int? = null

// Produce Option.Some
internal val someInt = Option.from(nullableInt)

// Produce Option.None
internal val noInt = Option.from(nullInt)

internal val someList = listOf(
    Option.Some(0),
    Option.Some(1),
    Option.Some(2),
)
internal val noneList = listOf<Option<Int>>(
    None,
    None,
)
internal val optionList = someList + noneList