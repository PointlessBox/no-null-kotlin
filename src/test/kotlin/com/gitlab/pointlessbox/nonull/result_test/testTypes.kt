package com.gitlab.pointlessbox.nonull.result_test

import com.gitlab.pointlessbox.nonull.Result
import java.lang.Exception

typealias ResultResult = Result<Result<Int, Exception>, Result<Int, Exception>>