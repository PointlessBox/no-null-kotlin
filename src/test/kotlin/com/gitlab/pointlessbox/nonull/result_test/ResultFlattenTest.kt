package com.gitlab.pointlessbox.nonull.result_test

import com.gitlab.pointlessbox.nonull.*
import org.junit.Test
import kotlin.test.assertEquals

class ResultFlattenTest {

    @Test
    fun given_anOkResultMappingToAnotherResult_when_flattened_then_theInnerResultShouldBeReturned() {
        val okMapped = okInt.mapOk { okInt }.flattenOk()
        assertEquals(okInt, okMapped)
    }

    @Test
    fun given_anErrResultMappingToAnotherResult_when_flattened_then_theInnerResultShouldBeReturned() {
        val okMapped = errInt.mapErr { okInt }.flattenErr()
        assertEquals(okInt, okMapped)
    }

    @Test
    fun given_aResultWhichMapsBothSidesToOtherResults_when_flattened_theTheInnerResultShouldBeReturned() {
        val okMapped: ResultResult = okInt.mapOk { okInt }
        val errMapped: ResultResult = errInt.mapErr { okInt }
        assertEquals(okInt, okMapped.flatten())
        assertEquals(okInt, errMapped.flatten())
    }
}