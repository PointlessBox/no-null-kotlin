package com.gitlab.pointlessbox.nonull.result_test

import com.gitlab.pointlessbox.nonull.*
import org.junit.Test
import kotlin.test.assertEquals

class ResultTest {

    @Test
    fun typeAliases_shouldProduceTheCorrectInstances() {
        assert(Ok(1).isOk())
        assert(Err(1).isErr())
    }

    @Test
    fun isOk_shouldReturnTrueWhenOkResultAndFalseIfErrResult() {
        assert(okInt.isOk())
        assert(!errInt.isOk())
    }

    @Test
    fun isErr_shouldReturnFalseWhenOkResultAndFalseIfErrResult() {
        assert(errInt.isErr())
        assert(!okInt.isErr())
    }

    @Test
    fun ifOk_shouldAlwaysReturnAnOption() {
        val noneFromErr = errInt.ifOk {
            1
        }
        val someFromOk = okInt.ifOk {
            2
        }
        assert(noneFromErr is Option.None)
        assert(someFromOk is Option.Some<Int>)
    }

    @Test
    fun ifOk_shouldRun_whenCalledOnAOkResult() {
        var wasCalled = false
        okInt.ifOk {
            wasCalled = true
        }
        assert(wasCalled)
    }

    @Test
    fun ifOk_shouldNotRun_whenCalledOnAErrResult() {
        var wasCalled = false
        errInt.ifOk {
            // This should not run
            wasCalled = true
        }
        assert(!wasCalled)
    }

    @Test
    fun ifErr_shouldAlwaysReturnAnResult() {
        val someFromErr = errInt.ifErr {
            1
        }
        val noneFromOk = okInt.ifErr {
            2
        }
        assert(someFromErr is Option.Some<Int>)
        assert(noneFromOk is Option.None)
    }

    @Test
    fun ifErr_shouldNotRun_whenCalledOnAOkResult() {
        var wasCalled = false
        okInt.ifErr {
            // This should not run
            wasCalled = true
        }
        assert(!wasCalled)
    }

    @Test
    fun ifErr_shouldRun_whenCalledOnAErrResult() {
        var wasCalled = false
        errInt.ifErr {
            wasCalled = true
        }
        assert(wasCalled)
    }

    @Test
    fun toOption_shouldReturn_aSomeOptionWhenCalledOnOKResult() = assert(okInt.toOption().isSome())

    @Test
    fun toOption_shouldReturn_aNoneOptionWhenCalledOnErrResult() = assert(errInt.toOption().isNone())

    @Test
    fun getOr_shouldReturnTheInnerOkValue_whenItIsAOkResult() {
        assertEquals(one, okInt.getOr { 2 })
        assertEquals(one, okInt.getOr(2))
    }

    @Test
    fun getOr_shouldReturnTheDefaultValue_whenItIsAErrResult() {
        val default = 2
        assertEquals(one, okInt.getOr { default })
        assertEquals(one, okInt.getOr(default))
    }

    @Test
    fun filterOk_shouldRemoveAllErrResults() {
        assert(resultList.filterOk().none { it.isErr() })
    }

    @Test
    fun filterOk_shouldNotRemoveAnyOkResults() {
        assertEquals(okList.size, resultList.filterOk().size)
    }

    @Test
    fun filterErr_shouldRemoveAllOkResults() {
        assert(resultList.filterErr().none { it.isOk() })
    }

    @Test
    fun filterErr_shouldNotRemoveAnyErrResults() {
        assertEquals(errList.size, resultList.filterErr().size)
    }

    @Test
    fun values_shouldReturnAllInnerValuesOfOkInstances() {
        assertEquals(listOf(0, 1, 2), resultList.values())
    }

    @Test
    fun errors_shouldReturnAllInnerErrorsOfErrInstances() {
        assertEquals(listOf(err, err), resultList.errors())
    }
}
