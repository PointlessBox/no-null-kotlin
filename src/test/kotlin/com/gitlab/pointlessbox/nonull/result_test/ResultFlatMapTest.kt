package com.gitlab.pointlessbox.nonull.result_test

import com.gitlab.pointlessbox.nonull.*
import org.junit.Test
import kotlin.test.assertEquals

class ResultFlatMapTest {

    @Test
    fun given_anOkResultMappingToAnotherResult_when_flattened_then_theInnerResultShouldBeReturned() {
        val okMapped = okInt.flatMapOk { okInt }
        assertEquals(okInt, okMapped)
    }

    @Test
    fun given_anErrResultMappingToAnotherResult_when_flattened_then_theInnerResultShouldBeReturned() {
        val okMapped = errInt.flatMapErr { okInt }
        assertEquals(okInt, okMapped)
    }
}