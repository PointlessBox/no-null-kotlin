package com.gitlab.pointlessbox.nonull.result_test

import com.gitlab.pointlessbox.nonull.Result

internal val one = 1
internal val err = Exception()

// Produce Result.Ok
internal val okInt = Result.Ok(one)

// Produce Result.Err
internal val errInt = Result.Err(err)
internal val okList = listOf(
    Result.Ok(0),
    Result.Ok(1),
    Result.Ok(2),
)
internal val errList = listOf(
    Result.Err(err),
    Result.Err(err),
)
internal val resultList = okList + errList