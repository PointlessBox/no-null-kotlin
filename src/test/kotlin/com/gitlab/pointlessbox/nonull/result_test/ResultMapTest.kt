package com.gitlab.pointlessbox.nonull.result_test

import com.gitlab.pointlessbox.nonull.Err
import com.gitlab.pointlessbox.nonull.Ok
import com.gitlab.pointlessbox.nonull.mapErr
import com.gitlab.pointlessbox.nonull.mapOk
import org.junit.Test
import kotlin.test.assertEquals

class ResultMapTest {
    @Test
    fun given_anOkResult_when_mapped_then_aNewResultWithTheMappedValueShouldBeReturned() {
        val mapped = okInt.mapOk { it.toString() }
        assertEquals(one.toString(), (mapped as Ok).value)
    }

    @Test
    fun given_anErrResult_when_mapped_then_aNewResultWithTheMappedErrorShouldBeReturned() {
        val s = "Something"
        val mapped = errInt.mapErr { s }
        assertEquals(s, (mapped as Err).err)
    }
}