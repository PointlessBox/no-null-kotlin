package com.gitlab.pointlessbox.nonull.result_test

import com.gitlab.pointlessbox.nonull.*
import org.junit.Test
import java.lang.Exception
import java.lang.IllegalStateException
import java.lang.NullPointerException
import java.util.concurrent.CancellationException
import kotlin.test.assertEquals
import kotlin.test.assertFails

class ResultBuilderTest {

    @Test
    fun resultBuilder_shouldCatchAllExceptionsExceptForCancellationException() {
        assertFails {
            result {
                throw CancellationException()
            }
        }
        assert(result { throw Exception() }.isErr())
    }

    @Test
    fun resultBuilder_shouldReturnAOkIfNoExceptionIsThrown() {
        assertEquals(1, result { 1 }.getOr(0))
    }

    @Test
    fun raiseBuilder_shouldNotCatchSubClassesOfTheGivenErr() {
        class SomeException : IllegalStateException()
        assertFails {
            raise(IllegalStateException::class) {
                throw SomeException()
            }
        }
        assertFails {
            raise(IllegalStateException::class) {
                throw IllegalStateException()
            }
        }
    }

    @Test
    fun raiseBuilder_shouldReturnAOkIfNoExceptionIsThrown() {
        assertEquals(1, raise { 1 }.getOr(0))
    }

    @Test
    fun raiseBuilder_shouldNotCatchCancellationException() {
        assertFails {
            raise {
                throw CancellationException()
            }
        }
    }

    @Test
    fun catchBuilder_shouldNotCatchCancellationException() {
        assertFails {
            catch {
                throw CancellationException()
            }
        }
    }

    @Test
    fun catchBuilder_shouldOnlyCatchTheGivenErr() {
        assertFails {
            catch(IllegalStateException::class) {
                null!!
            }
        }
        assert(catch(NullPointerException::class) { null!! }.isErr())
    }

    @Test
    fun mapErrBuilder_shouldReturnTheMappedErrorValueIfItThrowsTheRightException() {
        val errOne = mapErr(
            IllegalStateException::class to 1,
            NullPointerException::class to 2,
        ) { throw IllegalStateException() }
        assertEquals(1, (errOne as Err).err)

        val errTwo = mapErr(
            IllegalStateException::class to 1,
            NullPointerException::class to 2,
        ) { throw NullPointerException() }
        assertEquals(2, (errTwo as Err).err)
    }

    @Test
    fun mapErrBuilder_shouldNotCatchCancellationException() {
        assertFails {
            mapErr(CancellationException::class to 1) {
                throw CancellationException()
            }
        }
    }

    @Test
    fun mapErrBuilder_shouldNotCatchExceptionsThatAreNotPassed() {
        assertFails {
            mapErr(NullPointerException::class to 1) {
                throw IllegalStateException()
            }
        }
    }
}