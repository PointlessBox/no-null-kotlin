# No Null Kotlin

Rust-like Option and Result types for safer null-value and error handling.

# Examples

## Basic usage

### Option

```kotlin
fun makeSafe(nullableInt: Int?): Option<Int> {
    return if (nullableInt != null) Option.Some(nullableInt)
    else Option.None
}

when (val maybeInt = makeSafe(/* some nullable int */)) {
    is Option.Some -> {
        val someInt = maybeInt.value
        // do something with [someInt]
    }
    is Option.None -> {
        // do something else
    }
}
```

### Result

```kotlin
// Declare typealias to be less verbose
typealias ExResult<T> = Result<T, Throwable>

fun mayFail(): ExResult<Int> {
    return try {
        // Some action that might fail
        Result.Ok(/* some value */)
    } catch (ex: Throwable) {
        Result.Err(ex)
    }
}

when (val maybeInt = mayFail()) {
    is Result.Ok -> {
        val okInt = maybeInt.value
        // do something with [okInt]
    }
    is Result.Err -> {
        val err = maybeInt.err
        // do something with [err]
    }
}
```

# Supported by

[<img src="https://www.ics-group.eu/de/assets/images/ics-group-logo-desktop.svg" />](https://www.ics-group.eu/)